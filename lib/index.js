#!/usr/bin/env node
const path = require("path");
const program = require("commander");
const figlet = require("figlet");
const chalk = require("chalk");
const inquirer = require("inquirer");
const shell = require('shelljs');
const ora = require('ora');
const download = require('download-git-repo');
const pkg = require(path.resolve(__dirname, '../package.json'));
const exists = require('fs').existsSync;
const rm = require('rimraf').sync;

/**
 * 打印帮助信息
 * **/
function printHelp() {
    program.parse(process.argv)
    if (program.args.length < 1) return program.help()
}

/**
 * 打印支持模板的信息
 * **/
function consoleListTemplate() {
    console.log('')
    console.log(chalk.magenta('  basics -- 基于React的企业级管理系统'))
    console.log(chalk.magenta('  wxSmall -- 基于vant/weapp的微信小程序应用'))
    console.log('')
}

/**
 * 下载服务器远程模板
 * */
function downloadTemplate(temp, dirName) {
    const spinner = ora({
        text: '模板下载中...'
    })
    let gitUrl = `https://gitee.com/qmys1991/${temp}.git#master`
    spinner.start()
    download(`direct:${gitUrl}`, dirName, { clone: true }, function (err) {
        spinner.stop() //隐藏加载状态
        if(err) {
            console.log(`Failed to download ${err.message.trim()}`)
        } else {
            let pwd = shell.pwd();
            console.log(`项目地址：${pwd}/${dirName}/`);
            console.log('接下来你可以：');
            console.log('');
            console.log(chalk.blue(`    $ cd ${dirName}`));
            console.log(chalk.blue(`    $ yarn install`));
            console.log(chalk.blue(`    $ yarn start`));
            console.log('');
        }
    })
}


program
    .version(pkg.version, '-v, --version')
    .command("init")
    .alias('i')
    .description('请选择模版')
    .action(function() {
        figlet("鱼鳞图农业CLI",(err, data)=> {
            //先检查是否需要升级
            if(data) {
                console.log(chalk.red(data))
            }
            console.log('目前支持以下模板:');
            consoleListTemplate()
            const prompt = inquirer.createPromptModule();
            prompt({
                type: 'list',
                name: 'type',
                message: '模板类型',
                default: 'basics -- 基于React的企业级管理系统',
                choices: [
                    'basics -- 基于React的企业级管理系统',
                    'wxSmall -- 基于vant/weapp的微信小程序应用'
                ],
            }).then((res)=> {
                const type = res.type.split(' ')[0];
                prompt({
                    type: 'input',
                    name: 'project',
                    message: '项目名称:'
                }).then((iRes) => {
                    if(exists(iRes.project)) {
                        prompt({
                            type: 'confirm',
                            name: 'rmDir',
                            message: '存在此项目,继续将删除该项目？'
                        }).then((isDelete)=> {
                            if(isDelete.rmDir) {
                                rm(iRes.project)  //如果存在对应的项目名称，就删除
                                downloadTemplate(type, iRes.project)
                            }
                        })
                    } else {
                        downloadTemplate(type, iRes.project)
                    }
                })
            })
        })
    })
    .on('--help', ()=> {
        console.log(' Examples:')
        console.log()
        console.log(' nyCli init')
        console.log()
    })



printHelp()
